//library??XD
module reset_delay(input clock_from_main, output reg reset_over);
	
	reg	[32:0]	temp;
	
	initial begin
		temp <= 0;
		reset_over <= 1'b0;
	end
	
	always @(posedge clock_from_main) begin
		if(temp < 28'hFFFFFFF) begin
			temp <= temp + 1'b1;
			reset_over <= 1'b0;
		end
		else begin
			reset_over <= 1'b1;
		end
	end
	
endmodule