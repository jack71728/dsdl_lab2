module lcd_converter
(
	input [3:0] decimal,
	input clock_from_main, 
	input [4:0]next_coming, //1 when next number coming
	input lcd_prepare_ready, //0 is not ready
	output [7:0] LCD_DATA,
	output LCD_RS, LCD_EN, 
	output reg LCD_READY, //can start to transfer nums
	output reg LCD_write_over
);
	
	parameter converter_DELAY = 262144; //enough? 262144 is messured  at clock=250KHz ?
	reg converter_RS, converter_start;
	reg [1:0] converter_state;
	reg [7:0] LCD_DATA_to_controller;
	reg [32:0] converter_delay;
	reg [2:0] LCD_prepare_state;
	
	wire controllerDone;
		
	lcd_controller QAQ
	(
		.DATA_from_conv(LCD_DATA_to_controller),
		.clock(clock_from_main), 
		.inRS(converter_RS), 
		.control_start(converter_start), 
		.lcd_ready(lcd_prepare_ready), //1 is ready
		//output
		.out_DATA(LCD_DATA), 
		.LCD_RS(LCD_RS), 
		.LCD_EN(LCD_EN), 
		.Done(controllerDone)
	);

	always @(posedge clock_from_main)begin
		if(!lcd_prepare_ready) begin
			converter_RS <= 1'b0;
			converter_start <= 1'b0;
			converter_state <= 2'b00;
			LCD_DATA_to_controller <= 8'b0011_1000;
			converter_delay <= 32'h0000_0000;
			LCD_prepare_state <= 2'b00;	
			LCD_READY <= 1'b0; //can start eating nums
			LCD_write_over <= 1'b0; //num is written on display, wants next one
		end
		else begin
			if(!LCD_READY) begin
				case(converter_state)
					0: begin
						if(LCD_prepare_state == 0) begin
							LCD_DATA_to_controller <= 8'b0011_1000; //data_length=8bit, display_lines = 2
						end
						else if(LCD_prepare_state == 1) begin
							LCD_DATA_to_controller <= 8'b0000_1100; //turn on display and turn off cursor
						end
						else if(LCD_prepare_state == 2) begin
							LCD_DATA_to_controller <= 8'b0000_0001; //clear display
						end
						else if(LCD_prepare_state == 3) begin
							LCD_DATA_to_controller <= 8'b0000_0110; //cursor increments
						end
						else begin
							LCD_READY <= 1'b1; //ensuring
						end
						converter_start <= 1'b1;
						converter_RS <= 1'b0;
						converter_state <= 1;
					end
					1: begin
						if(controllerDone)begin
							converter_start <= 0;
							converter_state <= 2;
						end
					end
					2: begin //for delay
						if(converter_delay < converter_DELAY) begin
							converter_delay <= converter_delay+1'b1;
						end
						else begin
							converter_delay <= 0;
							converter_state <= 3;
						end
					end
					3: begin	
						converter_state <= 0;
						if(LCD_prepare_state == 3) begin
							LCD_READY <= 1'b1;
						end	
						else begin
							LCD_prepare_state <= LCD_prepare_state + 1;
						end
					end
				endcase
			end
			else begin
				case(converter_state)
					0: begin
						if(next_coming) begin
							case(decimal)
								0: LCD_DATA_to_controller <= 8'b0011_0000;
								1: LCD_DATA_to_controller <= 8'b0011_0001;
								2: LCD_DATA_to_controller <= 8'b0011_0010;
								3: LCD_DATA_to_controller <= 8'b0011_0011;
								4: LCD_DATA_to_controller <= 8'b0011_0100;
								5: LCD_DATA_to_controller <= 8'b0011_0101;
								6: LCD_DATA_to_controller <= 8'b0011_0110;
								7: LCD_DATA_to_controller <= 8'b0011_0111;
								8: LCD_DATA_to_controller <= 8'b0011_1000;
								9: LCD_DATA_to_controller <= 8'b0011_1001;
								10: LCD_DATA_to_controller <= 8'b0011_1010; //colon : 
								11: LCD_DATA_to_controller <= 8'b0011_1110; //>
								12: LCD_DATA_to_controller <= 8'b0010_1110; //.
								13: LCD_DATA_to_controller <= 8'b0000_0001; //clear display
								14: LCD_DATA_to_controller <= 8'b1000_0000; //Sets the DD RAM, the cursor's add to 000_0000, data is sent and recived after this setting
								15: LCD_DATA_to_controller <= 8'b1100_0000; //newline
							endcase
							
							if(decimal <= 12) begin //not a command
								converter_RS <= 1'b1;
							end
							else begin
								converter_RS <= 1'b0;
							end
							converter_start <= 1;
							converter_state <= 1;
							LCD_write_over <= 1'b0;
						end
						else begin
							LCD_write_over <= 1'b1;
						end
					end
					1: begin
						if(controllerDone) begin
							converter_start <= 0;
							converter_state <= 2;
						end
					end
					2: begin //delaying
						if(converter_delay < converter_DELAY) begin
							converter_delay <= converter_delay+1'b1;
						end
						else begin
							converter_delay <= 0;
							converter_state <= 3;
						end
					end
					3: begin
						converter_state <= 0;
						LCD_write_over <= 1'b1;
					end
				endcase
			end
		end
	end//always

endmodule

	
/*
0: DATA_buffer <= 0011_1000;//data_length=8bit, display_lines = 2
				1: DATA_buffer <= 0000_1100;//turn on display and turn off cursor
				2: DATA_buffer <= 0000_0001;//clear display
				3: DATA_buffer <= 0000_0110;//cursor increments
				4: DATA_buffer <= 1000_0000;//Sets the DDRAM, the cursor's add to 000_0000, data is sent and recived after this setting
				
1100_0000 //newline
*/

//http://esd.cs.ucr.edu/labs/interface/interface.html
//http://mil.ufl.edu/4744/docs/lcdmanual/commands.html#Rbf
//http://web.alfredstate.edu/weimandn/lcd/lcd_initialization/lcd_initialization_index.html

