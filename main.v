module main
(
	input run,//0 is pressed
	input record,
	input reset_current,
	input reset_record, 
	input clock_50Mhz,
	
	output [7:0] LEDP, //point
	output [6:0] out_seg7, out_seg6, out_seg5, out_seg4,
	output [6:0] out_seg3, out_seg2, out_seg1, out_seg0,
//	LCD Module 16X2
	output LCD_ON, // LCD Power ON/OFF
//	output LCD_BLON, // LCD Back Light ON/OFF DE2 doesn't have itQQ3
	output LCD_RW, // LCD Read/Write Select, 0 = Write, 1 = Read
	output LCD_EN, // LCD Enable
	output LCD_RS, // LCD Command/Data Select, 0 = Command, 1 = Data
	inout [7:0] LCD_DATA, // LCD Data bus 8 bits
//testing	
	output [17:0] testing
);
		
//state vars
	reg run_pressed;//1 for pressed;
	reg record_pressed;
	reg reset_current_pressed;
	reg reset_record_pressed;
	reg flag, flag1;//record
	
//time vars
	reg [7:0] hour, minute, second, centisecond;//8 bit max is enough
	reg [31:0] count;
	
//7seg vars
	reg [3:0] temp7, temp6, temp5, temp4;
	reg [3:0] temp3, temp2, temp1, temp0;
	
//LCD vars
	//starting delay
	parameter MAIN_DELAY=128;
	wire lcd_prepare_over; //restart time
	wire lcd_initial_over; //preprocessing
	wire lcd_updated; //updated current data
	reg [4:0] lcd_needs_update;
	reg [15:0] main_delay;
	reg [3:0] LCD_num[31:0];
	reg [3:0] LCD_buffer, LCD_write_state;

//7 seg assignment

	assign LEDP = 8'b1111_1011;
//	assign LEDP = LCD_DATA;
	
	seven_seg seg7(.in(temp7), .bright(1'b0), .out(out_seg7));
	seven_seg seg6(.in(temp6), .bright(1'b0), .out(out_seg6));
	seven_seg seg5(.in(temp5), .bright(1'b0), .out(out_seg5));
	seven_seg seg4(.in(temp4), .bright(1'b0), .out(out_seg4));
	seven_seg seg3(.in(temp3), .bright(1'b0), .out(out_seg3));
	seven_seg seg2(.in(temp2), .bright(1'b0), .out(out_seg2));
	seven_seg seg1(.in(temp1), .bright(1'b0), .out(out_seg1));
	seven_seg seg0(.in(temp0), .bright(1'b0), .out(out_seg0));
	
//LCD assignment
	
	reset_delay blablabla(.clock_from_main(clock_50Mhz), .reset_over(lcd_prepare_over));
	
	lcd_converter QAQ_DSDL
	(
		.decimal(LCD_buffer),
		.clock_from_main(clock_50Mhz), 
		.next_coming(lcd_needs_update), //1 when next number coming
		.lcd_prepare_ready(lcd_prepare_over), //0 is not ready
	//out
		.LCD_DATA(LCD_DATA),
		.LCD_RS(LCD_RS), 
		.LCD_EN(LCD_EN), 
		.LCD_READY(lcd_initial_over), //can start to transfer nums
		.LCD_write_over(lcd_updated)
	);
	
	assign LCD_ON = (lcd_prepare_over)? 1'b1 : 1'b0;
	assign LCD_RW = 1'b0;
//	assign LCD_BLON = (lcd_prepare_over)? 1'b1 : 1'b0;

//testing 1.2.7.8.10.11.12 11000 01101110
//								1				1						1			1			0		0		1	10110000			01101110	
	assign testing = {lcd_prepare_over, lcd_initial_over, {3{1'b1}}, LCD_ON, /*LCD_BLON*/ 1'b0, LCD_RW, LCD_EN, LCD_RS, LCD_DATA };
//
	initial begin
		//button reset => state reset
			run_pressed <= 1'b0;
			record_pressed <= 1'b0;
			reset_current_pressed <= 1'b0;
			reset_record_pressed <= 1'b0;
			
		//reset time vars
			count <= 32'h0000_0000;
			hour <= 8'h00;
			minute <= 8'h00;
			second <= 8'h00;
			centisecond <= 8'h00;
		
		//7seg vars
			temp7 <= 4'b0000;
			temp6 <= 4'b0000;
			temp5 <= 4'b0000;
			temp4 <= 4'b0000;
			temp3 <= 4'b0000;
			temp2 <= 4'b0000;
			temp1 <= 4'b0000;
			temp0 <= 4'b0000;
			
		//lcd var
			LCD_num[31] <= 4'b0000;//unused
			LCD_num[30] <= 4'b0000;//unused
			//LCD_num[29] <= 4'b0000;//unused
			
			LCD_num[29] <= 4'b0000;//t0
			LCD_num[28] <= 4'b0000;//t1
			LCD_num[27] <= 12;//.
			LCD_num[26] <= 4'b0000;//t2
			LCD_num[25] <= 4'b0000;//t3
			LCD_num[24] <= 10;//:
			LCD_num[23] <= 4'b0000;//t4
			LCD_num[22] <= 4'b0000;//t5
			LCD_num[21] <= 10;//:
			LCD_num[20] <= 4'b0000;//t6
			LCD_num[19] <= 4'b0000;//t7
			LCD_num[18] <= 11;//>
			LCD_num[17] <= 2;//second
			LCD_num[16] <= 15;//newline
			
			LCD_num[15] <= 4'b0000;//t0
			LCD_num[14] <= 4'b0000;//t1
			LCD_num[13] <= 12;//.
			LCD_num[12] <= 4'b0000;//t2
			LCD_num[11] <= 4'b0000;//t3
			LCD_num[10] <= 10;//:
			LCD_num[9] <= 4'b0000;//t4
			LCD_num[8] <= 4'b0000;//t5
			LCD_num[7] <= 10;//:
			LCD_num[6] <= 4'b0000;//t6
			LCD_num[5] <= 4'b0000;//t7
			LCD_num[4] <= 11;//>
			LCD_num[3] <= 1;//first one
			LCD_num[2] <= 14;//Sets the DDRAM, the cursor's add to 000_0000, data is sent and recived after this setting
			LCD_num[1] <= 13;//clear display
			LCD_num[0] <= 0;
			
			LCD_buffer <= 4'b0000;
			lcd_needs_update <= 1;//update lcd
			LCD_write_state <= 0;
			flag <= 0;
			flag1 <= 0;
	end
	
	always @( negedge run ) begin
		run_pressed <= ~run_pressed;
	end
	always @( record ) begin
		record_pressed <= ~record;
	end
	always @( reset_current ) begin
		reset_current_pressed <= ~reset_current;
	end
	always @( reset_record ) begin
		reset_record_pressed <= ~reset_record;
	end
	
	always @( posedge clock_50Mhz ) begin
		
		//clock work
		//update 7segs
			temp7 <= ((hour-(hour%10))/10)%10;
			temp6 <= hour%10;
			
			temp5 <= ((minute-(minute%10))/10)%10;
			temp4 <= minute%10;
			
			temp3 <= ((second-(second%10))/10)%10;
			temp2 <= second%10;
			
			temp1 <= ((centisecond-(centisecond%10))/10)%10;
			temp0 <= centisecond%10;
	
		if(lcd_prepare_over && lcd_initial_over) begin
		
		//current running and reset current
			if(run_pressed && !reset_current_pressed) begin
				if(count == 499999) begin
					count <= 0;
					if(centisecond == 99) begin
						centisecond <= 0;
						if(second == 59) begin
							second <= 0;
							if(minute == 59) begin
								minute <= 0;
								hour <= hour + 1;
							end
							else begin
								minute <= minute + 1;
							end
						end
						else begin
							second <= second+1;
						end
					end
					else begin
						centisecond <= centisecond + 1;
					end
				end
				else begin
					count <= count + 1;
				end
			end
			
			if(reset_current_pressed) begin
				count <= 0;
				hour <= 0;
				minute <= 0;
				second <= 0;
				centisecond <= 0;
			end
	
		//recording and reset record
			if(record_pressed && (~flag) && (~reset_record_pressed)) begin
				flag1 <= 1;
			end
			else if(~record_pressed) begin
				flag <= 0;
			end
			
			if((~record_pressed) && reset_record_pressed) begin
				LCD_num[19] <= 4'b0000;
				LCD_num[5] <= 4'b0000;
				LCD_num[20] <= 4'b0000;
				LCD_num[6] <= 4'b0000;
				LCD_num[22] <= 4'b0000;
				LCD_num[8] <= 4'b0000;
				LCD_num[23] <= 4'b0000;
				LCD_num[9] <= 4'b0000;
				LCD_num[25] <= 4'b0000;
				LCD_num[11] <= 4'b0000;
				LCD_num[26] <= 4'b0000;
				LCD_num[12] <= 4'b0000;
				LCD_num[28] <= 4'b0000;
				LCD_num[14] <= 4'b0000;
				LCD_num[29] <= 4'b0000;
				LCD_num[15] <= 4'b0000;
				lcd_needs_update <= 1;//update lcd
				LCD_write_state <= 0;
			end
			
			if(flag1)begin
				LCD_num[5] <= LCD_num[19];
				LCD_num[19] <= temp7;
				LCD_num[6] <= LCD_num[20];
				LCD_num[20] <= temp6;
				LCD_num[8] <= LCD_num[22];
				LCD_num[22] <= temp5;
				LCD_num[9] <= LCD_num[23];
				LCD_num[23] <= temp4;
				LCD_num[11] <= LCD_num[25];
				LCD_num[25] <= temp3;
				LCD_num[12] <= LCD_num[26];
				LCD_num[26] <= temp2;
				LCD_num[14] <= LCD_num[28];
				LCD_num[28] <= temp1;
				LCD_num[15] <= LCD_num[29];
				LCD_num[29] <= temp0;
				lcd_needs_update <= 1;//update lcd
				LCD_write_state <= 0;
				flag <= 1;
				flag1 <= 0;
			end
			
			if(lcd_needs_update > 0) begin				
				case(LCD_write_state)
					0: begin
						LCD_buffer <= LCD_num[lcd_needs_update];
						LCD_write_state <= 1;
						main_delay <= 0;
					end
					1: begin
						if(lcd_updated)begin
							LCD_write_state <= 2;
						end
					end
					2: begin //for delay
						if(main_delay < MAIN_DELAY)
							main_delay <= main_delay + 1;
						else begin
							LCD_write_state <= 3;
							main_delay <= 0;
						end
					end
					3: begin	
						LCD_write_state <= 0;
						if(lcd_needs_update < 29) begin
							lcd_needs_update <= lcd_needs_update + 1;
						end	
						else begin
							lcd_needs_update <= 0;
						end
					end
				endcase
			end	
			
			
		end//if(lcd_prepare_over)
	end//always posedge clock

endmodule

/*
0000_0001;//clear display
1000_0000;//Sets the DDRAM, the cursor's add to 000_0000, data is sent and recived after this setting
1>t7t6:t5t4:t3t2.t1t0
1100_0000 //newline
2>t7t6:t5t4:t3t2.t1t0

0: LCD_DATA_to_controller <= 0011_0000;
1: LCD_DATA_to_controller <= 0011_0001;
2: LCD_DATA_to_controller <= 0011_0010;
3: LCD_DATA_to_controller <= 0011_0011;
4: LCD_DATA_to_controller <= 0011_0100;
5: LCD_DATA_to_controller <= 0011_0101;
6: LCD_DATA_to_controller <= 0011_0110;
7: LCD_DATA_to_controller <= 0011_0111;
8: LCD_DATA_to_controller <= 0011_1000;
9: LCD_DATA_to_controller <= 0011_1001;
10: LCD_DATA_to_controller <= 0011_1010; //colon : 
11: LCD_DATA_to_controller <= 0011_1110; //>
12: LCD_DATA_to_controller <= 0010_1110; //.
13: LCD_DATA_to_controller <= 0000_0001; //clear display
14: LCD_DATA_to_controller <= 1000_0000; //Sets the DD RAM, the cursor's add to 000_0000, data is sent and recived after this setting
15: LCD_DATA_to_controller <= 1100_0000; //newline

*/


