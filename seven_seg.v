module seven_seg(in, bright, out);
	input [3:0] in;
	input bright;
	output [6:0] out;
//	wire bright;
	
//	assign bright = 0;
	assign out[0] = ( in[3] | in[1] | (in[0]&in[2]) | (~in[0]&~in[2]) )? bright:~bright;
	assign out[1] = ( (~in[1]&~in[0]) | in[2] | in[3] )? bright:~bright;
	assign out[2] = ( ~in[2] | in[3] | (~in[1]&~in[0]) | (in[1]&in[0]) )? bright:~bright;
	assign out[3] = ( in[3] | (in[2]&~in[1]) | (in[1]&~in[0]) | (~in[3]&~in[2]&in[1]) )? bright:~bright;
	assign out[4] = ( (~in[2]&~in[0]) | (in[1]&~in[0]) )? bright:~bright;
	assign out[5] = ( in[3] | in[2] | ~in[1] | (~in[3]&in[0]) )? bright:~bright;
	assign out[6] = ( in[3] | (~in[2]&~in[0]) | (in[1]&~in[0]) | (in[2]&~in[1]&in[0]) | (~in[2]&in[1]) )? bright:~bright;
	
endmodule