module lcd_controller
(
	input [7:0] DATA_from_conv,
	input clock, 
	input inRS, 
	input control_start, 
	input lcd_ready, //1 is ready
	output [7:0] out_DATA, 
	output LCD_RS, 
	output reg LCD_EN, 
	output reg Done
);
	
	assign out_DATA = DATA_from_conv;
	assign LCD_RS = inRS; //bypass RS
	
	parameter DELAY_TIME = 16;
	
	reg [1:0] lcd_state;
	reg [32:0] delay;
	reg preStart, temp_Start;
	
	always @(posedge clock)begin
		if(!lcd_ready) begin
			Done <= 1'b0;
			LCD_EN <= 1'b0;
			preStart<= 1'b0;
			temp_Start<= 1'b0;
			delay <= 0;
			lcd_state <= 0;
		end
		else begin
			//////	Input Start Detect ///////
			preStart <= control_start;
			if({preStart,control_start}==2'b01) begin
				temp_Start <= 1'b1;
				Done <= 1'b0;
			end
			//////////////////////////////////
			if(temp_Start) begin
				case(lcd_state)
					0: begin
						lcd_state <= 1;
					end
					1: begin
						LCD_EN <= 1'b1;
						lcd_state <= 2;
					end
					2: begin
						if(delay < DELAY_TIME)
							delay <= delay + 1;
						else
							lcd_state <= 3;
					end
					3: begin
						LCD_EN <= 1'b0;
						temp_Start <= 1'b0;
						Done <= 1'b1;
						delay <= 0;
						lcd_state <= 0;
					end
				endcase
			end
		end
	end//always

endmodule